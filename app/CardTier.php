<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardTier extends Model
{
  public function card()
  {
    return $this->hasMany(Card::class);
  }
}
