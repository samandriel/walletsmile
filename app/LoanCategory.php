<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanCategory extends Model
{
  public function loanType()
  {
    return $this->belongsTo(LoanType::class);
  }

  public function loan()
  {
    return $this->hasOne(Loan::class);
  }

}
