<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{
  public function card()
  {
    return $this->hasMany(Card::class);
  }
}
