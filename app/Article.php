<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function articleCategory()
    {
      return $this->belongsTo(ArticleCategory::class);
    }

    protected $guarded = [];
}
