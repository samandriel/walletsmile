<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
  public function cardType()
  {
    return $this->belongsTo(CardType::class);
  }

  public function cardTier()
  {
    return $this->belongsTo(CardTier::class);
  }

  public function coBrand()
  {
    return $this->belongsTo(coBrand::class);
  }

  protected $guarded = [];
}
