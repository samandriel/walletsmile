<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanType extends Model
{
  public function loanCategory()
  {
    return $this->hasOne(LoanCategory::class);
  }

  public function loan()
  {
    return $this->hasOne(Loan::class);
  }
}
