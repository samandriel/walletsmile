<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
  public function loanType()
  {
    return $this->belongsTo(LoanType::class);
  }

  public function loanCategory()
  {
    return $this->belongsTo(LoanCategory::class);
  }

  public function repaymentOption()
  {
    return $this->hasMany(RepaymentOption::class);
  }
  
  protected $guarded = [];
}
