<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoBrand extends Model
{
  public function card()
  {
    return $this->hasMany(Card::class);
  }
}
