<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinancialInstitution extends Model
{
  public function fiCategory()
  {
    return $this->belongsTo(FICategory::class);
  }

  public function country()
  {
    return $this->belongsTo(Country::class);
  }

  protected $guarded = [];
}
