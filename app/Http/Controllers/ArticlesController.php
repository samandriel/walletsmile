<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\ArticleCategory;
use App\Http\Requests\StoreArticle;
use App\Http\Requests\UpdateArticle;
use Image;
use Auth;

class ArticlesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin'); // (auth:guard_name)
    }

    public function index()
    {
        $articles = Article::orderBy('updated_at', 'desc')->paginate(10);
        return view('articles.index')->with('articles', $articles);//, compact('articles'));
    }

    public function show($id) //passing from route
    {
        $article = Article::find($id);

        return view('articles.show', compact('article'));
    }

    public function details($id) //passing from route
    {
        $article = Article::find($id);

        return view('articles.details', compact('article'));
    }

    public function create()
    {
        return view('articles.create');
    }

    public function store(StoreArticle $request)
    {
        $article = new Article($request->except('image', 'published'));
        $article->author_id = Auth::user()->id;
        $this->imageProcessing($article, $request);
        $this->publishStatus($article, $request);
        $article->save();

        return redirect()->route('articles.show', ['article' => $article->id]);
    }

    public function edit($id)
    {
        $article = Article::find($id);
        return view('articles.edit', compact('article'));
    }

    public function update($id, UpdateArticle $request)
    {
        $article = Article::find($id);
        $article->update($request->except('image', 'published'));
        $this->imageProcessing($article, $request);
        $this->publishStatus($article, $request);

        $article->save();

        return redirect()->route('articles.show', ['article' => $article->id]);
    }

    public function destroy($id)
    {
        $article = Article::find($id);
        if(isset($article->image)) {
            unlink(public_path('images/article/feature/'.$article->image));
        }
        $article->delete();

        return redirect()->route('articles.index');
    }

    private function imageProcessing(Article $article, Request $request)
    {
        if($request->hasFile('image'))
        {
            if(isset($article->image))
            {
                unlink(public_path('images/article/feature/'.$article->image));
            }
            $image = $request->file('image');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $image_location = public_path('images/article/feature/'.$filename);
            Image::make($image)->resize(250, 200)->save($image_location);
            $article->image = $filename;
        }
    }

    private function publishStatus(Article $article, Request $request)
    {
        $publishStatus = false;
        if($request->has('published')) {
            $publishStatus = true;
        }
        $article->published = $publishStatus;
    }
}
