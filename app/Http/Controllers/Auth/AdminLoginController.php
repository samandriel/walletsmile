<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function loginForm()
    {
        return view('auth.admin-login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);

        //dd($request->all());
        //attempt to log in
        // This method will return true or false
        // $remember is when admin choose 'remember me'
        // We need to specify guard otherwise it will use the default guard specified in config/auth.php
        $isAdmin = Auth::guard('admin')->attempt($request->only(['email', 'password']), $request->remember);
        if ($isAdmin) {
            // redirect to page that user want to go in order to redirect to the login page
            // Or if no intended page, it will redirect to the default route (admin.dashboard)
            return redirect()->intended(route('admin.dashboard'));
        };

        return redirect()->back()->withInput($request->only(['email', 'remember']));
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }

}
