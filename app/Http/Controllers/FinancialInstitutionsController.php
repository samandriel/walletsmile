<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FinancialInstitution;
use App\Http\Requests\StoreFinancialInstitution;
use App\Http\Requests\UpdateFinancialInstitution;
use Image;

class FinancialInstitutionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin'); // (auth:guard_name)
    }

    public function index()
    {
        $fis = FinancialInstitution::orderBy('updated_at', 'desc')->paginate(20);

        return view('financial_institutions.index')->with('fis', $fis);
    }

    public function create()
    {
        return view('financial_institutions.create');
    }

    public function store(StoreFinancialInstitution $request)
    {
        $fi = new FinancialInstitution($request->except('logo'));
        $this->imageProcessing($fi, $request);
        $fi->save();
        return redirect('financial_institution.show', ['id' => $fi->id]);
    }

    public function show($id)
    {
        $fi = FinancialInstitution::find($id);
        return view('financial_institutions.show')->with('fi', $fi);
    }

    public function edit($id)
    {
        $fi = FinancialInstitution::find($id);
        return view('financial_institutions.edit')->with('fi', $fi);
    }

    public function update(UpdateFinancialInstitution $request, $id)
    {
        $fi = FinancialInstitution::find($id);
        $fi->update($request->except('logo'));
        $this->imageProcessing($fi, $request);
        $fi->save();
        return redirect()->route('financial_institutions.show', ['fi' => $fi->id]);
    }

    public function destroy($id)
    {
        $fi = FinancialInstitution::find($id);
        unlink(public_path('images/fi/logo/'.$fi->logo));
        $fi->delete();
        return redirect()->route('financial_institutions.index');
    }

    private function imageProcessing(FinancialInstitution $fi, Request $request)
    {
        if($request->hasFile('logo'))
        {
            if(isset($fi->logo))
            {
                $oldImage = $fi->logo;
                unlink(public_path('images/fi/logo/'.$oldImage));
            }
            $image = $request->file('logo');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $image_location = public_path('images/fi/logo/'.$filename);
            Image::make($image)->resize(250, 200)->save($image_location);
            $fi->logo = $filename;
        }
    }

}
