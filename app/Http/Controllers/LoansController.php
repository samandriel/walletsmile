<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLoan;
use App\Http\Requests\UpdateLoan;
use App\Loan;

class LoansController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('details'); // (auth:guard_name)
    }

    public function index()
    {
        $loans = Loan::orderBy('updated_at', 'desc')->paginate(20);

        return view('loans.index')->with('loans', $loans);
    }

    public function create()
    {
        return view('loans.create');
    }

    public function store(StoreLoan $request)
    {
        $loan = new Loan($request->all());
        $this->isAvailableToExpat($loan, $request);
        $loan->save();
        return redirect()->route('loans.show', ['id' => $loan->id]);
    }

    public function show($id)
    {
        $loan = Loan::find($id);
        return view('loans.show')->with('loan', $loan);
    }

    public function details($id)
    {
        $loan = Loan::find($id);
        return view('loans.details')->with('loan', $loan);
    }

    public function edit($id)
    {
        $loan = Loan::find($id);
        return view('loans.edit', compact('loan'));
    }

    public function update(UpdateLoan $request, $id)
    {
        $loan = Loan::find($id);
        $loan->update($request->all());
        $this->isAvailableToExpat($loan, $request);
        $loan->save();

        return redirect()->route('loans.show', ['id' => $loan->id]);
    }

    public function destroy($id)
    {
        $loan = Loan::find($id)->delete();
        return redirect()->route('loans.index');
    }

    private function isAvailableToExpat(Loan $loan, Request $request)
    {
        $availableToExpat = false;
        if ($request->has('is_available_to_expat')) {
            $availableToExpat = true;
        }

        $loan->is_available_to_expat = $availableToExpat;
    }
}
