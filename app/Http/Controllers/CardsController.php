<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCard;
use App\Http\Requests\UpdateCard;
use App\Card;
use Image;

class CardsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin')->except('details'); // (auth:guard_name)
    }

    public function index()
    {
        $cards = Card::orderBy('updated_at', 'desc')->paginate(10);
        return view('cards.index')->with('cards', $cards);
    }


    public function create()
    {
        return view('cards.create');
    }

    public function store(StoreCard $request)
    {
        $card = new Card($request->except('image'));
        $this->imageProcessing($card, $request);
        $this->isAvailableToExpat($card, $request);
        $this->isCashCard($card, $request);
        $card->save();
        return redirect()->route('cards.show', ['id' => $card->id]);
    }


    public function show($id)
    {
        $card = Card::find($id);
        return view('cards.show')->with('card', $card);
    }

    public function details($id)
    {
        $card = Card::find($id);
        return view('cards.details')->with('card', $card);
    }

    public function edit($id)
    {
        $card = Card::find($id);
        return view('cards.edit')->with('card', $card);
    }


    public function update(UpdateCard $request, $id)
    {
        $card = Card::find($id);
        $card->update($request->except('image'));
        $this->imageProcessing($card, $request);
        $this->isAvailableToExpat($card, $request);
        $this->isCashCard($card, $request);
        $card->save();
        return redirect()->route('cards.show', ['id' => $card->id]);
    }

    public function destroy($id)
    {
        $card = Card::find($id);
        $this->removeImage($card);
        $card->delete();
        return redirect()->route('cards.index');
    }

    private function imageProcessing(Card $card, Request $request)
    {
        if($request->hasFile('image'))
        {
            $this->removeImage($card);

            $image = $request->file('image');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $image_location = public_path('images/card/'.$filename);
            Image::make($image)->resize(250, 200)->save($image_location);
            $card->image = $filename;
        }
    }

    private function removeImage(Card $card)
    {
        if(isset($card->image))
        {
            unlink(public_path('images/card/'.$card->image));
        }
    }

    private function isAvailableToExpat(Card $card, Request $request)
    {
        $availableToExpat = false;
        if ($request->has('is_available_to_expat')) {
            $availableToExpat = true;
        }
        $card->is_available_to_expat = $availableToExpat;
    }

    private function isCashCard(Card $card, Request $request)
    {
        $availableToExpat = false;
        if ($request->has('is_cash_card')) {
            $availableToExpat = true;
        }
        $card->is_cash_card = $availableToExpat;
    }
}
