<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;
use App\Loan;
use App\Article;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $credit_cards = Card::where('is_cash_card', false)->orderBy('updated_at', 'desc')->take(5)->get();
        $cash_cards = Card::where('is_cash_card', true)->orderBy('updated_at', 'desc')->take(5)->get();
        $home_loans = Loan::where('loan_type_id', 1)->orderBy('updated_at', 'desc')->take(5)->get();
        $auto_loans = Loan::where('loan_type_id', 2)->orderBy('updated_at', 'desc')->take(5)->get();
        $personal_loans = Loan::where('loan_type_id', 3)->orderBy('updated_at', 'desc')->take(5)->get();
        $latest_news = Article::where('article_category_id', 1)->orderBy('updated_at', 'desc')->take(5)->get();
        $latest_articles = Article::where('article_category_id', 2)->orderBy('updated_at', 'desc')->take(5)->get();
        return view('home', compact([
            'credit_cards',
            'cash_cards',
            'home_loans',
            'auto_loans',
            'personal_loans',
            'latest_articles',
            'latest_news'
        ]));
    }
}
