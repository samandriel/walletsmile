<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AdminsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin'); // (auth:guard_name)
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        // $admin = Auth::user();
        // $admin_name = $admin->first_name.' '.$admin->last_name;
        return view('admins.dashboard');
    }


}
