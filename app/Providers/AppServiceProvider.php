<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

// Models
use App\ArticleCategory;
use App\FiCategory;
use App\Admin;
use App\LoanType;
use App\LoanCategory;
use App\CardTier;
use App\CardType;

// Middleware
use Auth;
use Countries;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('articles.*', function($view) {
            $view->with([
                'article_categories' => ArticleCategory::all(),
                'authors' => Admin::all(),
            ]);
        });

        view()->composer('financial_institutions.*', function($view) {
            $view->with([
                'fi_categories' => FiCategory::all(),
                'countries' => Countries::all(),
            ]);
        });

        view()->composer('loans.*', function($view) {
            $view->with([
                'loan_types' => LoanType::all(),
                'loan_categories' => LoanCategory::all(),
            ]);
        });

        view()->composer('cards.*', function($view) {
            $view->with([
                'card_types' => CardType::all(),
                'card_tiers' => CardTier::all(),
            ]);
        });

        view()->composer('layouts.admin', function($view) {
            $admin = Auth::user();
            $admin_name = $admin->first_name.' '.$admin->last_name;
            $view->with([
                    'admin' => $admin,
                    'admin_name' => $admin_name,
                ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
