<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FiCategory extends Model
{
    public function financialInstitution()
    {
      return $this->hasMany(FinancialInstitution::class);
    }

    
}
