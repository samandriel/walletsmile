<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_cash_card')->default(false);
            $table->integer('card_type_id');
            $table->integer('card_tier_id');
            $table->integer('co_brand_id')->nullable();
            $table->string('benefit');
            $table->string('detail');
            $table->string('reward_redemption');
            $table->string('eligibility');
            $table->string('document_required');
            $table->boolean('is_available_to_expat')->default(false);
            $table->integer('min_age');
            $table->integer('min_income');
            $table->string('min_repayment');
            $table->string('interest_free_period');
            $table->integer('new_entry_fee_prime');
            $table->integer('new_entry_fee_add');
            $table->integer('annual_fee_prime');
            $table->integer('annual_fee_add');
            $table->string('misc_fee')->nullable();
            $table->string('slug')->unique();
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
