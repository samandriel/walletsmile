<?php

use Illuminate\Database\Seeder;

class LoanCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('loan_categories')->insert([
            ['id' => 1, 'name' => 'Residential Property', 'description' => 'n/a', 'loan_type_id' => 1],
            ['id' => 2, 'name' => 'Condomenium', 'description' => 'n/a', 'loan_type_id' => 1],
            ['id' => 3, 'name' => 'Refinance', 'description' => 'n/a', 'loan_type_id' => 1],
            ['id' => 5, 'name' => 'Mortgage', 'description' => 'n/a', 'loan_type_id' => 1]
        ]);
    }
}
