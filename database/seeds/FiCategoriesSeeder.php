<?php

use Illuminate\Database\Seeder;

class FiCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fi_categories')->insert([
            ['id' => 1, 'name' => 'Bank'],
            ['id' => 2, 'name' => 'Credit Card Company'],
            ['id' => 3, 'name' => 'Insurance Company'],
            ['id' => 4, 'name' => 'Leasing Company'],
            ['id' => 5, 'name' => 'Others']
        ]);
    }
}
