<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LoanCategoriesSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(FiCategoriesSeeder::class);

        DB::table('card_types')->insert([
            ['id' => 1, 'name' => 'Visa'],
            ['id' => 2, 'name' => 'MasterCard'],
            ['id' => 3, 'name' => 'JCB'],
            ['id' => 4, 'name' => 'American Express'],
            ['id' => 5, 'name' => 'UnionPay']
        ]);

        DB::table('card_tiers')->insert([
            ['id' => 1, 'name' => 'Standard'],
            ['id' => 2, 'name' => 'Classic'],
            ['id' => 3, 'name' => 'Titanium'],
            ['id' => 4, 'name' => 'Gold'],
            ['id' => 5, 'name' => 'Platinum'],
            ['id' => 6, 'name' => 'Signature'],
            ['id' => 7, 'name' => 'Infinite'],
            ['id' => 8, 'name' => 'Infinite Privilege'],
            ['id' => 9, 'name' => 'World'],
            ['id' => 10, 'name' => 'World Elite']
        ]);


        DB::table('loan_types')->insert([
            ['id' => 1, 'name' => 'Home Loan'],
            ['id' => 2, 'name' => 'Auto Loan'],
            ['id' => 3, 'name' => 'Personal Loan']
        ]);

        DB::table('article_categories')->insert([
            ['id' => 1, 'name' => 'Article'],
            ['id' => 2, 'name' => 'News']
        ]);

        DB::table('admins')->insert([
            ['id' => 1, 'first_name' => 'Harris', 'last_name' => 'Kunakira', 'email' => 'harris.kunakira@gmail.com', 'password' => Hash::make('freedom7')]
        ]);
    }
}
