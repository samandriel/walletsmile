<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group([
    'as' => 'admin.',
    'prefix' => 'admin'
], function () {
    Route::get('/login', 'Auth\AdminLoginController@loginForm')->name('login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('login.submit');
    Route::get('/', 'AdminsController@dashboard')->name('dashboard'); // this should come after
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('logout');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
});

Route::group([
    'prefix' => 'admin'
], function () {
    Route::resource('/financial_institutions', 'FinancialInstitutionsController');
    Route::resource('/cards', 'CardsController');
    Route::resource('/loans', 'LoansController');
    Route::resource('/articles', 'ArticlesController');
});

Route::get('/cards/{card}', 'CardsController@details')->name('cards.details');
Route::get('/loans/{loan}', 'LoansController@details')->name('loans.details');
Route::get('/articles/{article}', 'ArticlesController@details')->name('articles.details');

Route::get('/logout', 'Auth\LoginController@userLogout')->name('logout');
Route::get('/contact', 'StaticPagesController@contact');
Route::get('/about', 'StaticPagesController@about');
