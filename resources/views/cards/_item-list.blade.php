<div class="row">
    <div class="col-md-12">
        <div class="item-list">
            @foreach ($cards as $card)
                <div class="col-md-12 item">
                    <a href="{{ route('cards.details', ['card' => $card->id]) }}">
                        <ul>
                            <div class="col-md-3">
                                <li><img src="{{ $card->image != null ? asset('images/card/'.$card->image) : asset('no-image.png')}}" /></li>
                            </div>

                            <div class="col-md-9">
                                <div class="col-md-12 title">
                                    <li>{{ $card->name }}</li>
                                    <hr>
                                </div>

                                <div class="col-md-2">
                                    <li>{{ $card_types->find($card->card_type_id)->name }}</li>
                                </div>

                                <div class="col-md-2">
                                    <li>{{ $card_tiers->find($card->card_tier_id)->name }}</li>
                                </div>

                                <div class="col-md-2">
                                    <li>Min. Age: {{ $card->min_age }}</li>
                                </div>

                                <div class="col-md-3">
                                    <li>Min. Income: {{ $card->min_income }}</li>
                                </div>

                                <div class="col-md-3 pull-right">
                                    <li>Updated: {{ $card->updated_at->setTimezone('Asia/Bangkok')->diffForHumans() }}</li>
                                </div>

                                <div class="col-md-12">
                                    <br>
                                </div>

                                <div class="col-md-12 well">
                                    <li>Benefit: {{ $card->benefit }}</li>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr>
                            </div>
                        </ul>
                    </a>
                </div>

            @endforeach
        </div>
    </div>
</div>
