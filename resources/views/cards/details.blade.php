@extends ('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>{{ $card->name }}</h1>
            <hr>
            <p>{{ $card->id }}</p>

            <p>{{ $card_types->find($card->card_type_id)->name }}</p>
            <p>{{ $card_tiers->find($card->card_tier_id)->name }}</p>
            <p>{{ $card->max_credit_limit }}</p>
            <p>{{ $card->updated_at->setTimezone('Asia/Bangkok')->format('d/M/Y h:i:s A') }}</p>
            <p>


        </div>
    </div>
@endsection
