<form method="POST" action="{{ $action_path }}" enctype="multipart/form-data">
    {{ $method_field }}
    {{ csrf_field() }}

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" required value="{{ old('name',  isset($card->name) ? $card->name : null) }}">
    </div>

    <div class="form-group">
        <label for="slug">Slug</label>
        <input type="text" class="form-control" id="slug" name="slug" required value="{{ old('slug',  isset($card->slug) ? $card->slug : null) }}">
    </div>

    <div class="checkbox">
      <label>
        <input type="checkbox" name="is_cash_card" value='1' {{ old('is_cash_card', isset($card->is_cash_card) && $card->is_cash_card === 1 ? 'checked' : '') }} > This is Cash Card
      </label>
    </div>

    <div class="form-group">
        <label for="card_type">Type</label>
        <select class="form-control" id="card_type" name="card_type_id" >
            @if (isset($card->card_type_id ))
                <option value="{{ $card->card_type_id }}">{{ $card_types->find($card->card_type_id)->name }}</option>
                <option disabled="disabled">----------------</option>
            @endif
            @foreach ($card_types as $type)
                <option value="{{ $type->id }}">{{ $type->name }}</option>
            @endforeach
        </select>
    </div>


    <div class="form-group">
        <label for="card_tier">Tier</label>
        <select class="form-control" id="card_tier" name="card_tier_id" >
            @if (isset($card->card_tier_id ))
                <option value="{{ $card->card_tier_id }}">{{ $card_tiers->find($card->card_tier_id)->name }}</option>
                <option disabled="disabled">----------------</option>
            @endif
            @foreach ($card_tiers as $tier)
                <option value="{{ $tier->id }}">{{ $tier->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="benefit">Benefit</label>
        <textarea class="form-control" id="benefit" name="benefit" required>{{ old('benefit', isset($card->benefit) ? $card->benefit : null) }} </textarea>
    </div>

    <div class="form-group">
        <label for="detail">Detail</label>
        <textarea class="form-control" id="detail" name="detail" required>{{ old('detail', isset($card->detail) ? $card->detail : null) }} </textarea>
    </div>

    <div class="form-group">
        <label for="reward_redemption">Reward Redemption</label>
        <textarea class="form-control" id="reward_redemption" name="reward_redemption" required>{{ old('reward_redemption', isset($card->reward_redemption) ? $card->reward_redemption : null) }} </textarea>
    </div>

    <div class="form-group">
        <label for="eligibility">Eligibility</label>
        <textarea class="form-control" id="eligibility" name="eligibility" required>{{ old('eligibility', isset($card->eligibility) ? $card->eligibility : null) }} </textarea>
    </div>

    <div class="form-group">
        <label for="document_required">Document Required</label>
        <textarea class="form-control" id="document_required" name="document_required" required>{{ old('document_required', isset($card->document_required) ? $card->document_required : null) }} </textarea>
    </div>

    <div class="form-group">
        <label for="min_age">Minimum Age</label>
        <input type="text" class="form-control" id="min_age" name="min_age" required value="{{ old('min_age',  isset($card->min_age) ? $card->min_age : null) }}">
    </div>

    <div class="form-group">
        <label for="min_income">Minimum Income</label>
        <input type="text" class="form-control" id="min_income" name="min_income" required value="{{ old('min_income',  isset($card->min_income) ? $card->min_income : null) }}">
    </div>

    <div class="form-group">
        <label for="min_repayment">Minimum Repayment</label>
        <input type="text" class="form-control" id="min_repayment" name="min_repayment" required value="{{ old('min_repayment',  isset($card->min_repayment) ? $card->min_repayment : null) }}">
    </div>

    <div class="form-group">
        <label for="interest_free_period">Interest Free Period</label>
        <input type="text" class="form-control" id="interest_free_period" name="interest_free_period" required value="{{ old('interest_free_period',  isset($card->interest_free_period) ? $card->interest_free_period : null) }}">
    </div>

    <div class="form-group">
        <label for="new_entry_fee_prime">New Entry Fee (Primary Card)</label>
        <input type="text" class="form-control" id="new_entry_fee_prime" name="new_entry_fee_prime" required value="{{ old('new_entry_fee_prime',  isset($card->new_entry_fee_prime) ? $card->new_entry_fee_prime : null) }}">
    </div>

    <div class="form-group">
        <label for="new_entry_fee_add">New Entry Fee (Supplementary Card)</label>
        <input type="text" class="form-control" id="new_entry_fee_add" name="new_entry_fee_add" required value="{{ old('new_entry_fee_add',  isset($card->new_entry_fee_add) ? $card->new_entry_fee_add : null) }}">
    </div>

    <div class="form-group">
        <label for="annual_fee_prime">Annual Fee (Primary Card)</label>
        <input type="text" class="form-control" id="annual_fee_prime" name="annual_fee_prime" required value="{{ old('annual_fee_prime',  isset($card->annual_fee_prime) ? $card->annual_fee_prime : null) }}">
    </div>

    <div class="form-group">
        <label for="annual_fee_add">Annual Fee (Supplementary Card)</label>
        <input type="text" class="form-control" id="annual_fee_add" name="annual_fee_add" required value="{{ old('annual_fee_add',  isset($card->annual_fee_add) ? $card->annual_fee_add : null) }}">
    </div>


    <div class="form-group">
        <label for="misc_fee">Misc Fee</label>
        <textarea class="form-control" id="misc_fee" name="misc_fee" required>{{ old('misc_fee', isset($card->misc_fee) ? $card->misc_fee : null) }} </textarea>
    </div>

    <div class="checkbox">
      <label>
        <input type="checkbox" name="is_available_to_expat" value='1' {{ old('is_available_to_expat', isset($card->is_available_to_expat) && $card->is_available_to_expat === 1 ? 'checked' : '') }} > Available to Expat
      </label>
    </div>

    <div class="form-group">
        <label for="image">Image</label>
        <input type="file" class="form-control" id="image" name="image">
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>
