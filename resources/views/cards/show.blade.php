@extends('layouts.admin')

@section('page_name', '- {{ $card->name }}')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>{{ $card->name }}</h1>
            <hr>
            <p>{{ $card->id }}</p>

            {{-- <p>{{ $card_types->find($card->card_type_id)->name }}</p>
            <p>{{ $card_categories->find($card->card_category_id)->name }}</p>
            <p>{{ $card->max_credit_limit }}</p>
            <p>{{ $card->updated_at->setTimezone('Asia/Bangkok')->format('d/M/Y h:i:s A') }}</p>
            <p> --}}

            <form method="POST" action="{{ route('cards.destroy', ['card' => $card->id]) }}">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}

                <a href="{{ route('cards.index') }}" class="btn btn-primary"><i class="fa fa-list-alt" ></i> Dashboard</a>
                <a href="{{ route('cards.edit', ['card' => $card->id]) }}" class="btn btn-warning"> <i class="fa fa-edit" ></i> Edit</a>
                <button type="submit" class="btn btn-danger" ><i class="fa fa-trash" ></i> Delete</button>
            </form>

        </div>
    </div>
@endsection
