@extends('layouts.admin')

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Manage Cards</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <table id="datatable-checkbox" class="table table-striped bulk_action">
            <thead>
              <tr>

                {{-- <th><input type="checkbox" id="check-all" class="flat"></th> --}}
                <th>#ID</th>
                <th>Image</th>
                <th>Name</th>
                <th>Tier</th>
                <th>Cash Card</th>
                <th>Latest Update</th>
                <th>Action</th>
              </tr>
            </thead>

            @foreach ($cards as $card)
                <tbody>
                  <tr>
                    {{-- <td>
                    <input type="checkbox" id="check-all" class="flat">
                    </td> --}}
                    <td>{{ $card->id }}</td>
                    <td><img src="{{ $card->image != null ? asset('images/card/'.$card->image) : asset('no-image.png')}}" height="100px" width="150px" /></td>
                    <td>{{ $card->name }}</td>
                    <td>{{ $card_tiers->find($card->card_tier_id)->name }}</td>
                    <td>{{ $card->is_cash_card }}</td>
                    <td>{{ $card->updated_at->setTimezone('Asia/Bangkok')->format('d/M/Y h:i:s A') }}</td>
                    <td>
                        <form method="POST" action="{{ route('cards.update', ['card' => $card->id]) }}">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <a href="{{ route('cards.show', ['card' => $card->id]) }}" class="btn btn-info"><i class="fa fa-eye" ></i></a>
                            <a href="{{ route('cards.edit', ['card' => $card->id]) }}" class="btn btn-warning"><i class="fa fa-edit" ></i></a>
                            <button class="btn btn-danger" type="submit" ><i class="fa fa-trash" ></i></button>
                        </form>
                    </td>
                  </tr>
                </tbody>
            @endforeach
          </table>
          <div class="row">
              <div>Showing {{($cards->currentpage()-1)*$cards->perpage()+1}}-{{($cards->currentpage()-1)*$cards->perpage()+$cards->count()}} of {{$cards->total()}} entries
          </div>

          <div class="row">
              <div class="text-center" >{{ $cards->links() }}</div>
          </div>

            <div class="row">
                <p class="text-center"><a href="{{ route('cards.create') }}" class="btn btn-success"><i class="fa fa-plus" ></i> Create New Entry</a></p>
            </div>
        </div>

        </div>
      </div>
    </div>
@endsection
