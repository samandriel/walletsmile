@extends('layouts.admin')

@section('page_name', '- Edit Card')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>Edit Card</h1>
            <hr>

            @include('cards._form', [
                'action_path' => route('cards.update', ['card' => $card->id]),
                'method_field' => method_field('PUT'),
            ])

        </div>
    </div>

@endsection
