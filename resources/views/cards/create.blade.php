@extends('layouts.admin')

@section('page_name', '- Create Card')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>Create Card</h1>
            <hr>

            @include('cards._form', [
                'action_path' => route('cards.store'),
                'method_field' => null,
            ])

        </div>
    </div>

@endsection
