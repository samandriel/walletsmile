<form method="POST" action="{{ $action_path }}" enctype="multipart/form-data">
    {{ $method_field }}
    {{ csrf_field() }}

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" required value="{{ old('name',  isset($loan->name) ? $loan->name : null) }}">
    </div>

    <div class="form-group">
        <label for="slug">Slug</label>
        <input type="text" class="form-control" id="slug" name="slug" required value="{{ old('slug',  isset($loan->slug) ? $loan->slug : null) }}">
    </div>

    <div class="form-group">
        <label for="loan_type">Type</label>
        <select class="form-control" id="loan_type" name="loan_type_id" >
            @if (isset($loan->loan_type_id ))
                <option value="{{ $loan->loan_type_id }}">{{ $loan_types->find($loan->loan_type_id)->name }}</option>
                <option disabled="disabled">----------------</option>
            @endif
            @foreach ($loan_types as $type)
                <option value="{{ $type->id }}">{{ $type->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="loan_category">Category</label>
        <select class="form-control" id="loan_category" name="loan_category_id" >
            @if (isset($loan->loan_category_id))
                <option value="{{ $loan->loan_category_id }}">{{ $loan_categories->find($loan->loan_category_id)->name }}</option>
                <option disabled="disabled">----------------</option>
            @endif
            @foreach ($loan_categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="max_credit_limit">Maximum Credit Limit</label>
        <input type="text" class="form-control" id="max_credit_limit" name="max_credit_limit" required value="{{ old('max_credit_limit',  isset($loan->max_credit_limit) ? $loan->max_credit_limit : null) }}">
    </div>

    <div class="form-group">
        <label for="benefit">Benefit</label>
        <textarea class="form-control" id="benefit" name="benefit" required>{{ old('benefit', isset($loan->benefit) ? $loan->benefit : null) }} </textarea>
    </div>

    <div class="form-group">
        <label for="detail">Detail</label>
        <textarea class="form-control" id="detail" name="detail" required>{{ old('detail', isset($loan->detail) ? $loan->detail : null) }} </textarea>
    </div>

    <div class="form-group">
        <label for="eligibility">Eligibility</label>
        <textarea class="form-control" id="eligibility" name="eligibility" required>{{ old('eligibility', isset($loan->eligibility) ? $loan->eligibility : null) }} </textarea>
    </div>

    <div class="form-group">
        <label for="document_required">Document Required</label>
        <textarea class="form-control" id="document_required" name="document_required" required>{{ old('document_required', isset($loan->document_required) ? $loan->document_required : null) }} </textarea>
    </div>

    <div class="checkbox">
      <label>
        <input type="checkbox" name="is_available_to_expat" value='1' {{ old('is_available_to_expat', isset($loan->is_available_to_expat) && $loan->is_available_to_expat === 1 ? 'checked' : '') }} > Available to Expat
      </label>
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>
