@extends('layouts.admin')

@section('page_name', '- {{ $loan->name }}')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>{{ $loan->name }}</h1>
            <hr>
            <p>{{ $loan->id }}</p>
            <p>{{ $loan->name }}</p>
            <p>{{ $loan_types->find($loan->loan_type_id)->name }}</p>
            <p>{{ $loan_categories->find($loan->loan_category_id)->name }}</p>
            <p>{{ $loan->max_credit_limit }}</p>
            <p>{{ $loan->updated_at->setTimezone('Asia/Bangkok')->format('d/M/Y h:i:s A') }}</p>
            <p>

            <form method="POST" action="{{ route('loans.destroy', ['loan' => $loan->id]) }}">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}

                <a href="{{ route('loans.index') }}" class="btn btn-primary"><i class="fa fa-list-alt" ></i> Dashboard</a>
                <a href="{{ route('loans.edit', ['loan' => $loan->id]) }}" class="btn btn-warning"> <i class="fa fa-edit" ></i> Edit</a>
                <button type="submit" class="btn btn-danger" ><i class="fa fa-trash" ></i> Delete</button>
            </form>

        </div>
    </div>
@endsection
