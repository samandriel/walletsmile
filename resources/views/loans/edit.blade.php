@extends('layouts.admin')

@section('page_name', '- Edit Loan')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>Edit Loan</h1>
            <hr>

            @include('loans._form', [
                'action_path' => route('loans.update', ['loan' => $loan->id]),
                'method_field' => method_field('PUT'),
            ])

        </div>
    </div>

@endsection
