@extends('layouts.main')

@section('page_name', '- {{ $loan->name }}')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>{{ $loan->name }}</h1>
            <hr>
            {{-- <p>{{ $loan->id }}</p>
            <p>{{ $loan->name }}</p>
            <p>{{ $loan_types->find($loan->loan_type_id)->name }}</p>
            <p>{{ $loan_categories->find($loan->loan_category_id)->name }}</p>
            <p>{{ $loan->max_credit_limit }}</p>
            <p>{{ $loan->updated_at->setTimezone('Asia/Bangkok')->format('d/M/Y h:i:s A') }}</p>
            <p> --}}
        </div>
    </div>
@endsection
