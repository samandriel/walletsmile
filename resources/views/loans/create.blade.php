@extends('layouts.admin')

@section('page_name', '- Create Loan')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>Create Loan</h1>
            <hr>

            @include('loans._form', [
                'action_path' => route('loans.store'),
                'method_field' => null,
            ])

        </div>
    </div>

@endsection
