@extends('layouts.admin')

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Manage Loans</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <table id="datatable-checkbox" class="table table-striped bulk_action">
            <thead>
              <tr>

                {{-- <th><input type="checkbox" id="check-all" class="flat"></th> --}}
                <th>#ID</th>
                <th>Name</th>
                <th>Type</th>
                <th>Category</th>
                <th>Credit Limit</th>
                <th>Latest Update</th>
                <th>Action</th>
              </tr>
            </thead>

            @foreach ($loans as $loan)
                <tbody>
                  <tr>
                    {{-- <td>
                    <input type="checkbox" id="check-all" class="flat">
                    </td> --}}
                    <td>{{ $loan->id }}</td>
                    <td>{{ $loan->name }}</td>
                    <td>{{ $loan_types->find($loan->loan_type_id)->name }}</td>
                    <td>{{ $loan_categories->find($loan->loan_category_id)->name }}</td>
                    <td>{{ $loan->max_credit_limit }}</td>
                    <td>{{ $loan->updated_at->setTimezone('Asia/Bangkok')->format('d/M/Y h:i:s A') }}</td>
                    <td>
                        <form method="POST" action="{{ route('loans.update', ['loan' => $loan->id]) }}">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <a href="{{ route('loans.show', ['loan' => $loan->id]) }}" class="btn btn-info"><i class="fa fa-eye" ></i></a>
                            <a href="{{ route('loans.edit', ['loan' => $loan->id]) }}" class="btn btn-warning"><i class="fa fa-edit" ></i></a>
                            <button class="btn btn-danger" type="submit" ><i class="fa fa-trash" ></i></button>
                        </form>
                    </td>
                  </tr>
                </tbody>
            @endforeach
          </table>
          <div class="row">
              <div>Showing {{($loans->currentpage()-1)*$loans->perpage()+1}}-{{($loans->currentpage()-1)*$loans->perpage()+$loans->count()}} of {{$loans->total()}} entries
          </div>

          <div class="row">
              <div class="text-center" >{{ $loans->links() }}</div>
          </div>

            <div class="row">
                <p class="text-center"><a href="{{ route('loans.create') }}" class="btn btn-success"><i class="fa fa-plus" ></i> Create New Entry</a></p>
            </div>

        </div>
        </div>
      </div>
    </div>
@endsection
