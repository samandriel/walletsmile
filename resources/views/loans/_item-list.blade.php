<div class="row">
    <div class="col-md-12">
        <div class="item-list">
            @foreach ($loans as $loan)
                <div class="col-md-12 item">
                    <a href="{{ route('loans.details', ['loan' => $loan->id]) }}">
                        <ul>
                            <div class="col-md-3">
                                <li><img src="{{ asset('no-image.png') }}" /></li>
                            </div>

                            <div class="col-md-9">
                                <div class="col-md-12 title">
                                    <li>{{ $loan->name }}</li>
                                    <hr>
                                </div>

                                <div class="col-md-2">
                                    <li>Type: {{ $loan_types->find($loan->loan_type_id)->name }}</li>
                                </div>

                                <div class="col-md-2">
                                    <li>Category: {{ $loan_categories->find($loan->loan_category_id)->name }}</li>
                                </div>

                                <div class="col-md-2">
                                    <li>Max: Credit Limit: {{ $loan->max_credit_limit }}</li>
                                </div>

                                <div class="col-md-3">
                                    <li>Min. Income: {{ $loan->min_income }}</li>
                                </div>

                                <div class="col-md-3 pull-right">
                                    <li>Updated: {{ $loan->updated_at->setTimezone('Asia/Bangkok')->diffForHumans() }}</li>
                                </div>

                                <div class="col-md-12">
                                    <br>
                                </div>

                                <div class="col-md-12 well">
                                    <li>Benefit: {{ $loan->benefit }}</li>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr>
                            </div>
                        </ul>
                    </a>
                </div>

            @endforeach
        </div>
    </div>
</div>
