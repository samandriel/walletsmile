@extends('layouts.admin')

@section('page_name', '- {{$fi->name }}')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>{{ $fi->name }}</h1>
            <hr>

            <p>{{ $fi->id }}</p>
            <p><img src="{{ $fi->logo != null ? asset('images/fi/logo/'.$fi->logo) : asset('no-image.png')}}" height="100px" width="150px" /></p>
            <p>{{ $fi->name }}</p>
            <p>{{ $fi_categories->find($fi->fi_category_id)->name }}</p>
            <p>{{ $fi->address }}</p>
            <p>{{ $fi->city }}</p>
            <p>{{ $countries->find($fi->country_id)->name }}</p>
            <p>{{ $fi->website }}</p>
            <p>{{ $fi->phone_number }}</p>
            <p>{{ $fi->updated_at->setTimezone('Asia/Bangkok')->format('d/M/Y h:i:s A') }}</p>
            <p>

            <form method="POST" action="{{ route('articles.destroy', ['article' => $fi->id]) }}">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}

                <a href="{{ route('articles.index') }}" class="btn btn-primary"><i class="fa fa-list-alt" ></i> Dashboard</a>
                <a href="{{ route('articles.edit', ['article' => $fi->id]) }}" class="btn btn-warning"> <i class="fa fa-edit" ></i> Edit</a>
                <button type="submit" class="btn btn-danger" ><i class="fa fa-trash" ></i> Delete</button>
            </form>

        </div>
    </div>
@endsection
