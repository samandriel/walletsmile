@extends('layouts.admin')

@section('page_name', '- Edit Financial Institution')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>Edit Financial Institution</h1>
            <hr>

            @include('financial_institutions._form', [
                'action_path' => route('financial_institutions.update', ['financial_institutions' => $fi->id]),
                'method_field' => method_field('PUT'),
            ])

        </div>
    </div>

@endsection
