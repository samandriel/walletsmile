@extends('layouts.admin')

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Manage Financial Institutions</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <table id="datatable-checkbox" class="table table-striped bulk_action">
            <thead>
              <tr>

                {{-- <th><input type="checkbox" id="check-all" class="flat"></th> --}}
                <th>#ID</th>
                <th>Logo</th>
                <th>Name</th>
                <th>Category</th>
                <th>City</th>
                <th>Country</th>
                <th>Website</th>
                <th>Phone Number</th>
                <th>Latest Update</th>
                <th>Action</th>
              </tr>
            </thead>

            @foreach ($fis as $fi)
                <tbody>
                  <tr>
                    {{-- <td>
                    <input type="checkbox" id="check-all" class="flat">
                    </td> --}}
                    <td>{{ $fi->id }}</td>
                    <td><img src="{{ $fi->logo != null ? asset('images/fi/logo/'.$fi->logo) : asset('no-image.png')}}" height="100px" width="150px" /></td>
                    <td>{{ $fi->name }}</td>
                    <td>{{ $fi_categories->find($fi->fi_category_id)->name }}</td>
                    <td>{{ $fi->city }}</td>
                    <td>{{ $countries->find($fi->country_id)->name }}</td>
                    <td>{{ $fi->website }}</td>
                    <td>{{ $fi->phone_number }}</td>
                    <td>{{ $fi->updated_at->setTimezone('Asia/Bangkok')->format('d/M/Y h:i:s A') }}</td>
                    <td>
                        <form method="POST" action="{{ route('financial_institutions.update', ['financial_institution' => $fi->id]) }}">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <a href="{{ route('financial_institutions.show', ['financial_institution' => $fi->id]) }}" class="btn btn-info"><i class="fa fa-eye" ></i></a>
                            <a href="{{ route('financial_institutions.edit', ['financial_institution' => $fi->id]) }}" class="btn btn-warning"><i class="fa fa-edit" ></i></a>
                            <button class="btn btn-danger" type="submit" ><i class="fa fa-trash" ></i></button>
                        </form>
                    </td>
                  </tr>
                </tbody>
            @endforeach
          </table>
          <div class="row">
              <div>Showing {{($fis->currentpage()-1)*$fis->perpage()+1}}-{{($fis->currentpage()-1)*$fis->perpage()+$fis->count()}} of {{$fis->total()}} entries
          </div>

          <div class="row">
              <div class="text-center" >{{ $fis->links() }}</div>
          </div>

            <div class="row">
                <p class="text-center"><a href="{{ route('financial_institutions.create') }}" class="btn btn-success"><i class="fa fa-plus" ></i> Create New Entry</a></p>
            </div>

        </div>
        </div>
      </div>
    </div>
@endsection
