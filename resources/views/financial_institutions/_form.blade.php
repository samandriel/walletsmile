<form method="POST" action="{{ $action_path }}" enctype="multipart/form-data">
    {{ $method_field }}
    {{ csrf_field() }}

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" required value="{{ old('name',  isset($fi->name) ? $fi->name : null) }}">
    </div>

    <div class="form-group">
        <label for="fi_category">Category</label>
        <select class="form-control" id="fi_category" name="fi_category_id" >
            @if (isset($fi->fi_category_id))
                <option value="{{ $fi->fi_category_id }}">{{ $fi_categories->find($fi->fi_category_id)->name }}</option>
                <option disabled="disabled">----------------</option>
            @endif
            @foreach ($fi_categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="address">Address</label>
        <input type="text" class="form-control" id="address" name="address" required value="{{ old('address',  isset($fi->address) ? $fi->address : null) }}">
    </div>

    <div class="form-group">
        <label for="city">City</label>
        <input type="text" class="form-control" id="city" name="city" required value="{{ old('city',  isset($fi->city) ? $fi->city : null) }}">
    </div>

    <div class="form-group">
        <label for="country">Country</label>
        <select class="form-control" id="country" name="country_id" >
            @if (isset($fi->country_id))
                <option value="{{ $fi->country_id }}">{{ $countries->find($fi->country_id)->name }}</option>
                <option disabled="disabled">----------------</option>
            @endif
            @foreach ($countries as $country)
                <option value="{{ $country->id }}">{{ $country->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="zipcode">Zipcode</label>
        <input type="text" class="form-control" id="zipcode" name="zipcode" required value="{{ old('zipcode',  isset($fi->zipcode) ? $fi->zipcode : null) }}">
    </div>

    <div class="form-group">
        <label for="website">Website</label>
        <input type="text" class="form-control" id="website" name="website" required value="{{ old('website',  isset($fi->website) ? $fi->website : null) }}">
    </div>

    <div class="form-group">
        <label for="phone_number">Phone Number</label>
        <input type="text" class="form-control" id="phone_number" name="phone_number" required value="{{ old('phone_number',  isset($fi->phone_number) ? $fi->phone_number : null) }}">
    </div>

    <div class="form-group">
        <label for="logo">Logo</label>
        <input type="file" class="form-control" id="logo" name="logo">
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>
