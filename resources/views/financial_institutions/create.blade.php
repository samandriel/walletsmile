@extends('layouts.admin')

@section('page_name', '- Create New Financial Institution')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>Create New Financial Institution</h1>
            <hr>

            @include('financial_institutions._form', [
                'action_path' => route('financial_institutions.store'),
                'method_field' => null,
            ])

        </div>
    </div>

@endsection
