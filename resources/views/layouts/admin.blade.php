<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>WalletSmile.com @yield('page_name')</title>


        <!-- Latest compiled and minified CSS -->
        {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gentelella/1.3.0/css/custom.min.css" integrity="sha256-OvcgXxRONl1V26rs+1VRBgz6EReTjpFvyvIPzCjDu9I=" crossorigin="anonymous" />

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css" integrity="sha256-pMhcV6/TBDtqH9E9PWKgS+P32PVguLG8IipkPyqMtfY=" crossorigin="anonymous" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/green.css" integrity="sha256-5zuyx5fuDf6aU3/8tSuuR31yFxkMHjsTq43zd5dpNnU=" crossorigin="anonymous" />



        <!-- Latest compiled and minified JavaScript -->

    </head>


    <body class="nav-md">
        <div class="container body">
           <div class="main_container">
             <div class="col-md-3 left_col">
               <div class="left_col scroll-view">
                {{-- Brand --}}
                 <div class="navbar nav_title" style="border: 0;">
                   <a href="{{ route('admin.dashboard') }}" class="site_title"> <span>WalletSmile.com</span></a>
                 </div>

                 <div class="clearfix"></div>

                 <!-- menu profile quick info -->
                 <div class="profile clearfix">
                   <div class="profile_info">
                     <span>Welcome,</span>
                     <h2>{{ $admin_name }}</h2>
                   </div>
                 </div>
                 <!-- /menu profile quick info -->

                 <br />

                 <!-- sidebar menu -->
                 <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                   <div class="menu_section">
                     <h3>General</h3>
                     <ul class="nav side-menu">
                        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-home"></i> Home </span></a></li>
                        <li><a <a href="{{ route('cards.index') }}"><i class="fa fa-credit-card" ></i> Cards </span></a></li>
                        <li><a <a href="{{ route('loans.index') }}"><i class="fa fa-money"></i> Loans </span></a></li>
                        <li><a <a href="{{ route('financial_institutions.index') }}"><i class="fa fa-institution"></i> Financial Institutions </span></a></li>
                        <li><a href="{{ route('articles.index') }}"><i class="fa fa-file-text-o"></i> Articles </span></a></li>
                     </ul>

                     <h3>Users</h3>
                     <ul class="nav side-menu">
                        <li><a><i class="fa fa-user-o"></i> Manage User </span></a></li>
                        <li><a><i class="fa fa-user-circle"></i> Manage Admin </span></a></li>

                       {{-- <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                         <ul class="nav child_menu">
                           <li><a href="index.html">Dashboard</a></li>
                           <li><a href="index2.html">Dashboard2</a></li>
                           <li><a href="index3.html">Dashboard3</a></li>
                         </ul>
                       </li> --}}

                       {{-- <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li> --}}
                     </ul>
                   </div>

                 </div>
                 <!-- /sidebar menu -->

                 <!-- /menu footer buttons -->
                 {{-- <div class="sidebar-footer hidden-small">
                   <a data-toggle="tooltip" data-placement="top" title="Settings">
                     <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                   </a>
                   <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                     <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                   </a>
                 </div> --}}
                 <!-- /menu footer buttons -->
               </div>
             </div>

             <!-- top navigation -->
             <div class="top_nav">
               <div class="nav_menu">
                 <nav>
                   <div class="nav toggle">
                     <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                   </div>

                   <ul class="nav navbar-nav navbar-right">
                     <li class="">
                       <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                         {{ $admin_name }}
                         <span class=" fa fa-angle-down"></span>
                       </a>
                       <ul class="dropdown-menu dropdown-usermenu pull-right">
                         <li><a href="javascript:;"> Profile</a></li>
                         <li>
                           <a href="javascript:;">
                             <span>Settings</span>
                           </a>
                         </li>
                         <li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                       </ul>
                     </li>
                   </ul>
                 </nav>
               </div>
             </div>
             <!-- /top navigation -->

            {{-- Page Content --}}
            <div class="right_col" role="main">
                @yield('content')
            </div>
             <!-- footer content -->
             <footer>
               <div class="pull-right">
                 Copyright © 2017 <a href="{{ url('/') }}">WalletSmile.com</a>
               </div>
               <div class="clearfix"></div>
             </footer>
             <!-- /footer content -->
           </div>
         </div>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.js" integrity="sha256-GqCMs8eqcNJo0k1Zw3TBSve9COCvjIX45PYKJlH0urU=" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js" integrity="sha256-8HGN1EdmKWVH4hU3Zr3FbTHoqsUcfteLZJnVmqD/rC8=" crossorigin="anonymous"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/gentelella/1.3.0/js/custom.min.js" integrity="sha256-sbN5Px8w3bxIVMr78rm8N/Icnm4Wtbh8VgfJ8g+b130=" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.js" integrity="sha256-XWzSUJ+FIQ38dqC06/48sNRwU1Qh3/afjmJ080SneA8=" crossorigin="anonymous"></script>


    </body>
</html>
