<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright pull-right">
                  <span>Copyright © 2017 - <a href="{{ url('/') }}">WalletSmile.com</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
