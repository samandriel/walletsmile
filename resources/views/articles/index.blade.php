@extends('layouts.admin')

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Manage Articles</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <table id="datatable-checkbox" class="table table-striped bulk_action">
            <thead>
              <tr>

                {{-- <th><input type="checkbox" id="check-all" class="flat"></th> --}}
                <th>#ID</th>
                <th>Image</th>
                <th>Title</th>
                <th>Category</th>
                <th>Author</th>
                <th>Status</th>
                <th>Latest Updated</th>
                <th>Action</th>
              </tr>
            </thead>

            @foreach ($articles as $article)
                <tbody>
                  <tr>
                    {{-- <td>
                    <input type="checkbox" id="check-all" class="flat">
                    </td> --}}
                    <td>{{ $article->id }}</td>
                    <td><img src="{{ $article->image != null ? asset('images/article/feature/'.$article->image) : asset('no-image.png')}}" height="100px" width="150px" /></td>
                    <td>{{ $article->title }}</td>
                    <td>{{ $article_categories->find($article->article_category_id)->name }}</td>
                    <td>{{ $authors->find($article->author_id)->first_name }}</td>
                    <td>{{ $article->published == 1 ? "Published" : "Unpublished" }}</td>
                    <td>{{ $article->updated_at->setTimezone('Asia/Bangkok')->format('d/M/Y h:i:s A') }}</td>
                    <td>
                        <form method="POST" action="{{ route('articles.update', ['article' => $article->id]) }}">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <a href="{{ route('articles.show', ['article' => $article->id]) }}" class="btn btn-info"><i class="fa fa-eye" ></i></a>
                            <a href="{{ route('articles.edit', ['article' => $article->id]) }}" class="btn btn-warning"><i class="fa fa-edit" ></i></a>
                            <button class="btn btn-danger" type="submit" ><i class="fa fa-trash" ></i></button>
                        </form>
                    </td>
                  </tr>
                </tbody>
            @endforeach
          </table>
          <div class="row">
              <div>Showing {{($articles->currentpage()-1)*$articles->perpage()+1}}-{{($articles->currentpage()-1)*$articles->perpage()+$articles->count()}} of {{$articles->total()}} entries
          </div>

          <div class="row">
              <div class="text-center" >{{ $articles->links() }}</div>
          </div>

            <div class="row">
                <p class="text-center"><a href="{{ route('articles.create') }}" class="btn btn-success"><i class="fa fa-plus" ></i> Create New Entry</a></p>
            </div>

        </div>
        </div>
      </div>
    </div>
@endsection
