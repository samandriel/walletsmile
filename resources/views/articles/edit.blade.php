@extends('layouts.admin')

@section('page_name', '- Edit Article')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>Edit Article</h1>
            <hr>

            @include('articles._form', [
                'action_path' => route('articles.update', ['article' => $article->id]),
                'method_field' => method_field('PUT'),
            ])

        </div>
    </div>

@endsection
