<form method="POST" action="{{ $action_path }}" enctype="multipart/form-data">
    {{ $method_field }}
    {{ csrf_field() }}

    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" name="title" required value="{{ old('title',  isset($article->title) ? $article->title : null) }}">
    </div>

    <div class="form-group">
        <label for="slug">Slug</label>
        <input type="text" class="form-control" id="slug" name="slug" required value="{{ old('slug',  isset($article->slug) ? $article->slug : null) }}">
    </div>

    <div class="form-group">
        <label for="excerpt">Excerpt</label>
        <textarea class="form-control" id="excerpt" name="excerpt" required>{{ old('excerpt', isset($article->excerpt) ? $article->excerpt : null) }} </textarea>
    </div>

    <div class="form-group">
        <label for="content">Content</label>
        <textarea class="form-control" id="content" name="content" required>{{ old('content', isset($article->content) ? $article->content : null) }} </textarea>
    </div>

    <div class="form-group">
        <label for="category">Category</label>
        <select class="form-control" id="category" name="article_category_id" >
            @if (isset($article->article_category_id))
                <option value="{{ $article->article_category_id }}">{{ $article_categories->find($article->article_category_id)->name }}</option>
                <option disabled="disabled">----------------</option>
            @endif
            @foreach ($article_categories as $c)
                <option value="{{ $c->id }}">{{ $c->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="image">Feature Image</label>
        <input type="file" class="form-control" id="image" name="image">
    </div>

    <button type="submit" class="btn btn-default" >Save as draft</button>
    <button type="submit" class="btn btn-default" name="published" value='published' >Publish</button>
</form>
