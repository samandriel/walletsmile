@extends ('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>{{ $article->title }}</h1>
            <hr>
            <p>{{ $article->content }}</p>

        </div>
    </div>
@endsection
