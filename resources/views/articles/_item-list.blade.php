<div class="row">
    <div class="col-md-12">
        <div class="item-list">
            @foreach ($articles as $article)
                <div class="col-md-12 item">
                    <a href="{{ route('articles.details', ['article' => $article->id]) }}">
                        <ul>
                            <div class="col-md-5">
                                <li><img src="{{ $article->image != null ? asset('images/article/feature/'.$article->image) : asset('no-image.png')}}" /></li>
                            </div>

                            <div class="col-md-7">
                                <div class="col-md-12 title">
                                    <li>{{ $article->title }}</li>
                                    <hr>
                                </div>

                                <div class="col-md-12">
                                    <li>{{ $article->excerpt }}</li>
                                </div>

                                <div class="col-md-12 pull-right">
                                    <li>Updated: {{ $article->updated_at->setTimezone('Asia/Bangkok')->diffForHumans() }}</li>
                                </div>

                            </div>

                            <div class="col-md-12">
                                <hr>
                            </div>
                        </ul>
                    </a>
                </div>

            @endforeach
        </div>
    </div>
</div>
