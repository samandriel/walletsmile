@extends('layouts.admin')

@section('page_name', '- {{ $article->title }}')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>{{ $article->title }}</h1>
            <hr>
            <p>{{ $article->content }}</p>
            <hr>

            <form method="POST" action="{{ route('articles.destroy', ['article' => $article->id]) }}">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}

                <a href="{{ route('articles.index') }}" class="btn btn-primary"><i class="fa fa-list-alt" ></i> Dashboard</a>
                <a href="{{ route('articles.edit', ['article' => $article->id]) }}" class="btn btn-warning"> <i class="fa fa-edit" ></i> Edit</a>
                <button type="submit" class="btn btn-danger" ><i class="fa fa-trash" ></i> Delete</button>
            </form>

        </div>
    </div>
@endsection
