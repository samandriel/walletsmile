@extends('layouts.admin')

@section('page_name', '- Create New Article')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.errors')

            <h1>Create New Article</h1>
            <hr>

            @include('articles._form', [
                'action_path' => route('articles.store'),
                'method_field' => null,
            ])

        </div>
    </div>

@endsection
