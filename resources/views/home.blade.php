@extends('layouts.main')

@section('content')
    <div class="jumbotron">

            <div class="col-md-12">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h1>WalletSmile</h1>
                            <p>Discover Financial Products and Service from Financial Institutions in One Place</p>
                            <a class="btn btn-primary btn-md">Learn more</a>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                  <div class="panel-heading">
                    <h3 class="panel-title">Card</h3>
                  </div>
                  <div class="panel-body">

                      <ul class="nav nav-tabs blue-tabs">
                        <li class="active"><a href="#credit-cards" data-toggle="tab" aria-expanded="false">Credit Cards</a></li>
                        <li><a href="#cash-cards" data-toggle="tab" aria-expanded="true">Cash Cards</a></li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in" id="credit-cards">
                          @include('cards._item-list', ['cards' => $credit_cards])
                        </div>
                        <div class="tab-pane fade" id="cash-cards">
                          @include('cards._item-list', ['cards' => $cash_cards])
                        </div>
                      </div>

                  </div>
                </div>

                <div class="panel panel-info">
                  <div class="panel-heading">
                    <h3 class="panel-title">Loan</h3>
                  </div>
                  <div class="panel-body">
                      <ul class="nav nav-tabs blue-tabs">
                        <li class="active"><a href="#home-loans" data-toggle="tab" aria-expanded="true">Home Loans</a></li>
                        <li><a href="#auto-loans" data-toggle="tab" aria-expanded="false">Auto Loans</a></li>
                        <li><a href="#personal-loans" data-toggle="tab" aria-expanded="false">Personal Loans</a></li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in" id="home-loans">
                          @include('loans._item-list', ['loans' => $home_loans])
                        </div>
                        <div class="tab-pane fade" id="auto-loans">
                          @include('loans._item-list', ['loans' => $auto_loans])
                        </div>
                        <div class="tab-pane fade" id="personal-loans">
                          @include('loans._item-list', ['loans' => $personal_loans])
                        </div>
                      </div>
                  </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-success">
                  <div class="panel-heading">
                    <h3 class="panel-title">News</h3>
                  </div>
                  <div class="panel-body">
                    @include('articles._item-list', ['articles' => $latest_news])
                  </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-success">
                  <div class="panel-heading">
                    <h3 class="panel-title">Articles</h3>
                  </div>
                  <div class="panel-body">
                    @include('articles._item-list', ['articles' => $latest_articles])
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection
